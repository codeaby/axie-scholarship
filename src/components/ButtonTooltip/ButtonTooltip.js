import { ClickAwayListener, Tooltip } from '@material-ui/core';
import React from 'react';

const ButtonTooltip = ({ opened, onClose, title, children }) => {
  return (
    <ClickAwayListener onClickAway={onClose}>
      <div>
        <Tooltip
          PopperProps={{
            disablePortal: true,
          }}
          onClose={onClose}
          open={opened}
          disableFocusListener
          disableHoverListener
          disableTouchListener
          title={title}
        >
          {children}
        </Tooltip>
      </div>
    </ClickAwayListener>
  );
};

export default ButtonTooltip;
