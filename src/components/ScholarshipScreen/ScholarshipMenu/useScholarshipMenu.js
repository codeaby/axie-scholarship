import { useState } from 'react';

const useScholarshipMenu = (scholarshipId) => {
  const [openDelete, setOpenDelete] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [copied, setCopied] = useState(false);

  const handleDeleteTooltipClose = () => {
    setOpenDelete(false);
  };

  const handleDeleteTooltipOpen = () => {
    setOpenDelete(true);
  };

  const handleEditTooltipClose = () => {
    setOpenEdit(false);
  };

  const handleEditTooltipOpen = () => {
    setOpenEdit(true);
  };

  const shareScholarship = async (e) => {
    e.preventDefault();
    await navigator.clipboard.writeText(
      `https://codeaby.gitlab.io/axie-scholarship/public/scholarship/${scholarshipId}`
    );
    setCopied(true);
    setTimeout(() => setCopied(false), 2000);
  };

  return {
    handleEditTooltipOpen,
    handleEditTooltipClose,
    openEdit,
    copied,
    shareScholarship,
    handleDeleteTooltipOpen,
    handleDeleteTooltipClose,
    openDelete,
  };
};

export default useScholarshipMenu;
