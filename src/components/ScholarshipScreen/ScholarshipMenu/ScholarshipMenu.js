import { Box, IconButton } from '@material-ui/core';
import { Add, Delete, Edit, Share } from '@material-ui/icons';
import React from 'react';
import ButtonTooltip from '../../ButtonTooltip/ButtonTooltip';
import useScholarshipMenu from './useScholarshipMenu';

const ScholarshipMenu = ({ scholarshipId, showAddAxieDialog }) => {
  const {
    handleEditTooltipOpen,
    handleEditTooltipClose,
    openEdit,
    copied,
    shareScholarship,
    handleDeleteTooltipOpen,
    handleDeleteTooltipClose,
    openDelete,
  } = useScholarshipMenu(scholarshipId);

  return (
    <Box display="flex" justifyContent="center" marginTop={1}>
      <ButtonTooltip opened={openEdit} onClose={handleEditTooltipClose} title="Available soon...">
        <IconButton onClick={handleEditTooltipOpen}>
          <Edit />
        </IconButton>
      </ButtonTooltip>

      <ButtonTooltip opened={copied} onClose={() => {}} title="Copied to Clipboard!">
        <IconButton onClick={shareScholarship}>
          <Share />
        </IconButton>
      </ButtonTooltip>

      <ButtonTooltip
        opened={openDelete}
        onClose={handleDeleteTooltipClose}
        title="Available soon..."
      >
        <IconButton onClick={handleDeleteTooltipOpen}>
          <Delete />
        </IconButton>
      </ButtonTooltip>

      <IconButton onClick={showAddAxieDialog}>
        <Add />
      </IconButton>
    </Box>
  );
};

export default ScholarshipMenu;
