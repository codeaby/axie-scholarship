import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  TextField,
} from '@material-ui/core';
import React, { useState } from 'react';

const AddAxiesToScholarshipDialog = ({ open, onClose, save }) => {
  const [axies, setAxies] = useState('');

  const clear = () => {
    setAxies('');
  };

  const cleanAndClose = () => {
    clear();
    onClose();
  };

  const saveAndClose = async () => {
    await save(axies);
    cleanAndClose();
  };

  if (!open) return null;

  return (
    <Dialog open={open} onClose={cleanAndClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Add Axies to Scholarship</DialogTitle>
      <DialogContent style={{ minWidth: 300 }}>
        <DialogContentText>Add all Axie ids separated with commas</DialogContentText>
        <FormControl fullWidth style={{ marginBottom: 20 }}>
          <TextField
            multiline
            autoFocus
            margin="dense"
            id="name"
            label="Axie Ids"
            type="text"
            fullWidth
            value={axies}
            onChange={(e) => setAxies(e.target.value)}
          />
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button onClick={cleanAndClose} color="default">
          Cancel
        </Button>
        <Button onClick={saveAndClose} color="primary" variant="contained">
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddAxiesToScholarshipDialog;
