import { Group, ShoppingCart } from '@material-ui/icons';
import React from 'react';
import ScholarshipMenu from '../ScholarshipMenu/ScholarshipMenu';
import {
  ScholarshipHeaderContainer,
  ScholarshipName,
  ScholarshipNameAndPrice,
  ScholarshipTotalPrice,
  ScholarshipType,
} from '../ScholarshipScreenElements';

const ScholarshipHeader = ({ scholarship, total, totalUSD, usePublic, showAddAxieDialog }) => {
  const scholarshipTypeIcon = scholarship.type === 'scholarship' ? Group : ShoppingCart;

  return (
    <ScholarshipHeaderContainer>
      <ScholarshipNameAndPrice>
        <ScholarshipName variant="h6">
          <ScholarshipType component={scholarshipTypeIcon} />
          {scholarship.name}
        </ScholarshipName>
        <ScholarshipTotalPrice>
          ~ {total} ETH <br /> ~ {totalUSD} USD
        </ScholarshipTotalPrice>
      </ScholarshipNameAndPrice>

      {!usePublic && (
        <ScholarshipMenu showAddAxieDialog={showAddAxieDialog} scholarshipId={scholarship.id} />
      )}
    </ScholarshipHeaderContainer>
  );
};

export default ScholarshipHeader;
