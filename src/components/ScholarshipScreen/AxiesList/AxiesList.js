import { List, Typography } from '@material-ui/core';
import { Tune } from '@material-ui/icons';
import React from 'react';
import AxieListItem from '../AxieListItem/AxieListItem';
import { AxiesListContainer, FilterAxiesButton } from '../ScholarshipScreenElements';

const AxiesList = ({ scholarship, showAxieDialog }) => {
  return (
    <>
      <Typography variant="subtitle2">
        Total Axies: {scholarship.axies.length}
        <FilterAxiesButton size="small">
          <Tune />
        </FilterAxiesButton>
      </Typography>
      <AxiesListContainer>
        <List>
          {scholarship.axies.map((axie) => (
            <AxieListItem key={axie.id} axie={axie} onItemClick={showAxieDialog} />
          ))}
        </List>
      </AxiesListContainer>
    </>
  );
};

export default AxiesList;
