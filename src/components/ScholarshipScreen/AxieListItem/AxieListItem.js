import {
  Avatar,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
} from '@material-ui/core';
import React from 'react';

const AxieListItem = ({ axie, onItemClick }) => {
  return (
    <ListItem key={axie.id} onClick={() => onItemClick(axie)}>
      <ListItemAvatar>
        <Avatar
          style={{ width: 70, height: 70 }}
          src={`https://storage.googleapis.com/assets.axieinfinity.com/axies/${axie.id}/axie/axie-full-transparent.png`}
        />
      </ListItemAvatar>
      <ListItemText primary={`#${axie.id}`} />
      <ListItemSecondaryAction>
        <ListItemText
          primary={`~ ${axie.price ? axie.price.avgPrice : 'N/A'} ETH`}
          secondary={`~ ${axie.price ? axie.price.avgPriceUSD : 'N/A'} USD`}
        ></ListItemText>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default AxieListItem;
