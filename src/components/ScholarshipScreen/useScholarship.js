import { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import {
  getScholarship,
  getPublicScholarship,
  addAxiesToScholarship,
} from '../../services/AxieScholarshipService';

const useScholarship = (usePublic = false) => {
  const [loading, setLoading] = useState(true);
  const [scholarship, setScholarship] = useState(null);
  const { scholarshipId } = useParams();
  const [axieModalOpened, setAxieModalOpened] = useState(false);
  const [addAxieDialogOpened, setAddAxieDialogOpened] = useState(false);
  const [selectedAxie, setSelectedAxie] = useState(null);

  const loadScholarship = useCallback(async () => {
    setLoading(true);
    const { data } = await (usePublic
      ? getPublicScholarship(scholarshipId)
      : getScholarship(scholarshipId));
    setScholarship(data);
    setLoading(false);
  }, [usePublic, scholarshipId]);

  useEffect(() => {
    loadScholarship();
  }, [loadScholarship]);

  let totalUSD = 0;
  let total = 0;

  if (scholarship) {
    totalUSD = scholarship.axies
      .reduce((sum, axie) => sum + (axie.price ? axie.price.avgPriceUSD : 0), 0)
      .toFixed(2);
    total = scholarship.axies
      .reduce((sum, axie) => sum + (axie.price ? axie.price.avgPrice : 0), 0)
      .toFixed(3);
  }

  const showAxieDialog = (axie) => {
    setSelectedAxie(axie);
    setAxieModalOpened(true);
  };

  const closeAxieDialog = () => {
    setSelectedAxie(null);
    setAxieModalOpened(false);
  };

  const showAddAxieDialog = () => {
    setAddAxieDialogOpened(true);
  };

  const closeAddAxieDialog = () => {
    setAddAxieDialogOpened(false);
  };

  const addAxies = async (axies) => {
    await addAxiesToScholarship(scholarshipId, axies);
    await loadScholarship();
  };

  return {
    loading,
    scholarship,
    totalUSD,
    total,
    axieModalOpened,
    showAxieDialog,
    closeAxieDialog,
    addAxieDialogOpened,
    showAddAxieDialog,
    closeAddAxieDialog,
    selectedAxie,
    addAxies,
  };
};

export default useScholarship;
