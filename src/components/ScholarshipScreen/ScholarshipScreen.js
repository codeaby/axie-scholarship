import { MainLayout } from 'codeaby-framework';
import React from 'react';
import Footer from '../Footer/Footer';
import AddAxiesToScholarshipDialog from './AddAxiesToScholarshipDialog/AddAxiesToScholarshipDialog';
import AxieDetailsModal from './AxieDetailsModal/AxieDetailsModal';
import AxiesList from './AxiesList/AxiesList';
import ScholarshipHeader from './ScholarshipHeader/ScholarshipHeader';
import { ScholarshipScreenContainer } from './ScholarshipScreenElements';
import useScholarship from './useScholarship';

const ScholarshipScreen = ({ usePublic }) => {
  const {
    loading,
    scholarship,
    total,
    totalUSD,
    axieModalOpened,
    showAxieDialog,
    closeAxieDialog,
    selectedAxie,
    addAxieDialogOpened,
    showAddAxieDialog,
    closeAddAxieDialog,
    addAxies,
  } = useScholarship(usePublic);

  if (loading) {
    return (
      <MainLayout
        title="Axie Scholarships"
        publicScreen={usePublic}
        loading
        backButtonLink="/"
      ></MainLayout>
    );
  }

  return (
    <MainLayout title="Axie Scholarships" publicScreen={usePublic} backButtonLink="/">
      <ScholarshipScreenContainer>
        <ScholarshipHeader
          scholarship={scholarship}
          total={total}
          totalUSD={totalUSD}
          usePublic={usePublic}
          showAddAxieDialog={showAddAxieDialog}
        />
        <AxiesList scholarship={scholarship} showAxieDialog={showAxieDialog} />
      </ScholarshipScreenContainer>
      <AddAxiesToScholarshipDialog
        open={addAxieDialogOpened}
        onClose={closeAddAxieDialog}
        save={addAxies}
      />
      <AxieDetailsModal axie={selectedAxie} open={axieModalOpened} onClose={closeAxieDialog} />
      <Footer />
    </MainLayout>
  );
};

export default ScholarshipScreen;
