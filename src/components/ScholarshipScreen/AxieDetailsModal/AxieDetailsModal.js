import {
  Avatar,
  Box,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  styled,
  Typography,
} from '@material-ui/core';
import React from 'react';
import { ScholarshipTotalPrice } from '../ScholarshipScreenElements';
import { formatDistance } from 'date-fns';
import { Close } from '@material-ui/icons';
import Aquatic from './Aquatic.png';
import Beast from './Beast.png';
import Bird from './Bird.png';
import Bug from './Bug.png';
import Dawn from './Dawn.png';
import Dusk from './Dusk.png';
import Mech from './Mech.png';
import Plant from './Plant.png';
import Reptile from './Reptile.png';

const CLASS_ICONS = {
  Aquatic,
  Beast,
  Bird,
  Bug,
  Dawn,
  Dusk,
  Mech,
  Plant,
  Reptile,
};

const CloseButton = styled(IconButton)(({ theme }) => ({
  position: 'absolute',
  right: theme.spacing(1),
  top: theme.spacing(1),
  color: theme.palette.grey[500],
}));

const AxieDetailsModal = ({ axie, open, onClose }) => {
  if (!axie) return null;

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle disableTypography>
        <Typography variant="h5">
          <img style={{ width: 25, verticalAlign: 'middle' }} src={CLASS_ICONS[axie.class]} /> #
          {axie.id}
          <CloseButton aria-label="close" onClick={onClose}>
            <Close />
          </CloseButton>
        </Typography>
      </DialogTitle>
      <DialogContent dividers style={{ minWidth: 300 }}>
        <Typography variant="subtitle2">
          Breed: <b>{axie.breedCount}</b> - Hp: <b>{axie.stats.hp}</b> Sp: <b>{axie.stats.speed}</b>{' '}
          Sk: <b>{axie.stats.skill}</b> Mo: <b>{axie.stats.morale}</b>
        </Typography>
        <Box align="center">
          <Avatar
            style={{ width: 200, height: 200 }}
            src={`https://storage.googleapis.com/assets.axieinfinity.com/axies/${axie.id}/axie/axie-full-transparent.png`}
          />
        </Box>
        <ScholarshipTotalPrice>
          ~ {axie.price ? axie.price.avgPrice : 'N/A'} ETH <br /> ~{' '}
          {axie.price ? axie.price.avgPriceUSD : 'N/A'} USD
        </ScholarshipTotalPrice>
        <br />
        <Typography variant="subtitle2" align="center">
          Updated price:{' '}
          {axie.price && formatDistance(new Date(axie.price.date), new Date(), { addSuffix: true })}
        </Typography>
        <Typography variant="subtitle2" align="center">
          Similar axies: {axie.price ? axie.price.results : 'N/A'}
        </Typography>
      </DialogContent>
    </Dialog>
  );
};

export default AxieDetailsModal;
