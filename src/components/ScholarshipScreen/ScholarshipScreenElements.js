import { Box, IconButton, styled, SvgIcon, Typography } from '@material-ui/core';

export const ScholarshipScreenContainer = styled(Box)({
  minHeight: '75vh',
});

export const ScholarshipHeaderContainer = styled(Box)(({ theme }) => ({
  margin: theme.spacing(-3, -2, 2),
  padding: theme.spacing(2, 2, 1),
  backgroundColor: theme.palette.primary.dark,
  borderRadius: theme.spacing(0, 0, 3, 3),
  position: 'relative',
}));

export const ScholarshipNameAndPrice = styled(Box)({
  display: 'flex',
});

export const ScholarshipType = styled(SvgIcon)(({ theme }) => ({
  marginRight: theme.spacing(1),
}));

export const ScholarshipName = styled(Typography)(({ theme }) => ({
  paddingRight: theme.spacing(2),
  lineHeight: 1.2,
  flex: 1,
  display: 'flex',
  alignItems: 'center',
}));

export const ScholarshipTotalPrice = styled(({ ...others }) => (
  <Typography variant="subtitle1" align="center" {...others} />
))(() => ({
  lineHeight: 1.6,
  textAlign: 'right',
  display: 'flex',
  alignItems: 'center',
}));

export const FilterAxiesButton = styled(IconButton)({
  float: 'right',
});

export const AxiesListContainer = styled(Box)(({ theme }) => ({
  marginBottom: theme.spacing(5),
  borderRadius: theme.spacing(2),
}));
