import { Add } from '@material-ui/icons';
import { MainLayout } from 'codeaby-framework';
import React from 'react';
import Footer from '../Footer/Footer';
import { AddScholarshipButton } from './HomeScreenElements';
import NewScholarshipDialog from './NewScholarshipDialog/NewScholarshipDialog';
import ScholarshipsList from './ScholarshipsList/ScholarshipsList';
import useScholarships from './useScholarships';

const HomeScreen = () => {
  const { loading, scholarships, openModal, closeModal, modalOpened, createScholarship } =
    useScholarships();

  return (
    <MainLayout title="Axie Scholarships" loading={loading}>
      <ScholarshipsList scholarships={scholarships} />
      <NewScholarshipDialog open={modalOpened} onClose={closeModal} save={createScholarship} />
      <Footer />
      <AddScholarshipButton color="primary" aria-label="add" onClick={openModal}>
        <Add />
      </AddScholarshipButton>
    </MainLayout>
  );
};

export default HomeScreen;
