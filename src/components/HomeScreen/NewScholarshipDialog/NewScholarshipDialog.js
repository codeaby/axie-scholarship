import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@material-ui/core';
import React, { useState } from 'react';

const NewScholarshipDialog = ({ open, onClose, save }) => {
  const [type, setType] = useState('scholarship');
  const [name, setName] = useState('');

  const clear = () => {
    setType('scholarship');
    setName('');
  };

  const cleanAndClose = () => {
    clear();
    onClose();
  };

  const saveAndClose = async () => {
    await save(type, name);
    cleanAndClose();
  };

  if (!open) return null;

  return (
    <Dialog open={open} onClose={cleanAndClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Create New</DialogTitle>
      <DialogContent style={{ minWidth: 300 }}>
        <FormControl fullWidth style={{ marginBottom: 10 }}>
          <InputLabel>Type</InputLabel>
          <Select value={type} id="type" onChange={(e) => setType(e.target.value)} label="Type">
            <MenuItem value="scholarship">Scholarship</MenuItem>
            <MenuItem value="buy-list">Buy List</MenuItem>
          </Select>
        </FormControl>
        <FormControl fullWidth style={{ marginBottom: 20 }}>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="text"
            fullWidth
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button onClick={cleanAndClose} color="default">
          Cancel
        </Button>
        <Button onClick={saveAndClose} color="primary" variant="contained">
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default NewScholarshipDialog;
