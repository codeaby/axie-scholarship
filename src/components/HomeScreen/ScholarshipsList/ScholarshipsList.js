import { List } from '@material-ui/core';
import { AccountBalance, Group, ShoppingCart } from '@material-ui/icons';
import React from 'react';
import { useHistory } from 'react-router';
import { ScholarshipsListContainer } from '../HomeScreenElements';
import CollapsibleList from '../CollapsibleList/CollapsibleList';

const ScholarshipsList = ({ scholarships = [] }) => {
  const history = useHistory();
  const navigateToScholarship = (id) => {
    history.push(`/scholarship/${id}`);
  };

  const general = scholarships.filter((s) => s.type === 'scholarship');
  const buyList = scholarships.filter((s) => s.type === 'buy-list');
  const academies = [];

  return (
    <ScholarshipsListContainer>
      <List subheader={<li />}>
        <CollapsibleList
          key="scholarships"
          title="Scholarships"
          list={general}
          emptyListText="No scholarship added."
          onItemClick={navigateToScholarship}
          icon={<Group />}
        />
        <CollapsibleList
          key="buy-list"
          title="Buy Lists"
          list={buyList}
          emptyListText="No buy list added."
          onItemClick={navigateToScholarship}
          icon={<ShoppingCart />}
        />
        <CollapsibleList
          key="academies"
          title="Academies"
          list={academies}
          emptyListText="Available soon..."
          onItemClick={() => {}}
          icon={<AccountBalance />}
        />
      </List>
    </ScholarshipsListContainer>
  );
};

export default ScholarshipsList;
