import { Box, Fab, ListItem, styled } from '@material-ui/core';

export const AddScholarshipButton = styled(Fab)({
  position: 'fixed',
  margin: 0,
  right: 20,
  bottom: 80,
});

export const ScholarshipsListContainer = styled(Box)(({ theme }) => ({
  minHeight: '75vh',
  marginTop: theme.spacing(2),
  marginBottom: theme.spacing(5),
  borderRadius: theme.spacing(2),
}));

export const NestedItem = styled(ListItem)(({ theme }) => ({
  paddingLeft: theme.spacing(4),
}));
