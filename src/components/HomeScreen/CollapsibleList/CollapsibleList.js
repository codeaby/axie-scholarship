import { Collapse, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import React, { useState } from 'react';
import { NestedItem } from '../HomeScreenElements';

const CollapsibleList = ({ key, list, title, emptyListText, onItemClick, icon }) => {
  const [open, setOpen] = useState(!!list.length);

  return (
    <>
      <ListItem button key={key} onClick={() => setOpen(!open)}>
        <ListItemText>{title}</ListItemText>
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open}>
        <List component="div" disablePadding>
          {!list.length && <ListItem>{emptyListText}</ListItem>}
          {list.map((scholarship) => (
            <NestedItem key={scholarship.id} button onClick={() => onItemClick(scholarship.id)}>
              <ListItemIcon>{icon}</ListItemIcon>
              <ListItemText primary={scholarship.name} />
            </NestedItem>
          ))}
        </List>
      </Collapse>
    </>
  );
};

export default CollapsibleList;
