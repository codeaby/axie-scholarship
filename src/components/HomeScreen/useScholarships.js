import { useCallback, useEffect, useState } from 'react';
import { getUserScholarships, createScholarship } from '../../services/AxieScholarshipService';

const useScholarships = () => {
  const [loading, setLoading] = useState(true);
  const [scholarships, setScholarships] = useState([]);
  const [open, setOpen] = useState(false);

  const loadUserScholarships = useCallback(async () => {
    setLoading(true);
    const { data } = await getUserScholarships();
    setScholarships(data.scholarships);
    setLoading(false);
  }, []);

  useEffect(() => {
    loadUserScholarships();
  }, [loadUserScholarships]);

  const createAndReload = async (type, name) => {
    await createScholarship(type, name);
    await loadUserScholarships();
  };

  return {
    loading,
    scholarships,
    loadUserScholarships,
    openModal: () => setOpen(true),
    closeModal: () => setOpen(false),
    modalOpened: open,
    createScholarship: createAndReload,
  };
};

export default useScholarships;
