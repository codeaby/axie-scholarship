import axios from 'axios';
import { Authentication } from 'codeaby-framework';

const BASE_URL = 'https://us-central1-codeaby-486b9.cloudfunctions.net';

export const getPublicScholarship = async (id) =>
  axios.get(`${BASE_URL}/axiescholarship/public/scholarships/${id}`);

const getAxios = async () =>
  axios.create({
    baseURL: `${BASE_URL}/axiescholarship`,
    headers: await Authentication.getAuthorizationHeader(),
  });

export const getUserScholarships = async () => (await getAxios()).get('/scholarships');
export const createScholarship = async (type, name) =>
  (await getAxios()).post('/scholarships', { type, name });

export const getScholarship = async (id) => (await getAxios()).get(`/scholarships/${id}`);
export const deleteScholarship = async (id) => (await getAxios()).delete(`/scholarships/${id}`);

export const addAxiesToScholarship = async (id, axies) =>
  (await getAxios()).post(`/scholarships/${id}/axies`, { axies });

export const getAxie = async (id, axie) =>
  (await getAxios()).get(`/scholarships/${id}/axies/${axie}`);
export const removeAxie = async (id, axie) =>
  (await getAxios()).delete(`/scholarships/${id}/axies/${axie}`);
