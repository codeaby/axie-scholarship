import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Loading, LoginScreen, PrivateRoute, useAuth } from 'codeaby-framework';
import HomeScreen from '../components/HomeScreen/HomeScreen';
import ScholarshipScreen from '../components/ScholarshipScreen/ScholarshipScreen';

const Routing = () => {
  const { loaded } = useAuth();

  if (!loaded) return <Loading fullScreen />;

  return (
    <BrowserRouter basename="/axie-scholarship">
      <Switch>
        <Route path="/login">
          <LoginScreen />
        </Route>
        <Route path="/public/scholarship/:scholarshipId">
          <ScholarshipScreen usePublic />
        </Route>
        <PrivateRoute path="/scholarship/:scholarshipId">
          <ScholarshipScreen />
        </PrivateRoute>
        <PrivateRoute path="/" exact>
          <HomeScreen />
        </PrivateRoute>
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
  );
};

export default Routing;
