import Providers from './providers/Providers';
import Routing from './routing/Routing';

function App() {
  return (
    <Providers>
      <Routing />
    </Providers>
  );
}

export default App;
